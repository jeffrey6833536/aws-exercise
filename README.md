#### This project is for the DevOps Bootcamp exercise for

#### "AWS Services"

#### Project Title

CI/CD - Deploy Application from Jenkins Pipeline on EC2 Instance (automatically with docker-compose) via AWS CLI

#### Technologies used

AWS, Jenkins, Docker, Linux, Git, JavaScript, NodeJs, Docker Hub

#### Project Description

1. Installed and configured AWS CLI tool to connect to AWS account
2. Created EC2 Instance using the AWS CLI with all necessary configurations like Security Group
3. Created SSH key pair, IAM resources like User, Group, Policy using the AWS CLI
4. Prepared AWS EC2 Instance for deployment (Install Docker)
5. Created ssh key credentials for EC2 server on Jenkins
6. Extended the previous CI pipeline (jenkins-exercise) with deploy step to ssh into the remote EC2 instance and deploy newly built     image from Jenkins server.
7. Configured security group on EC2 Instance to allow access to our web application
8. CI step:Increment version
9. CI step: Build artifact for NodJs application
10. CI step: Build and push Docker image to Docker Hub
11. CD step: Deploy new application version with Docker Compose
12. CD step: Commit the version update
